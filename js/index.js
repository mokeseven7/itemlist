//------DOM VARS------//
//cache the add item item
var form = document.getElementById('addForm');
//cache the item list <ul>
var itemList = document.getElementById('items');
//cache the search items input
var filter = document.getElementById('filter');



//------EVENTS------//
//form submit
form.addEventListener('submit', addItem);
//delete btn clicked on an item
itemList.addEventListener('click', removeItem);
//enter key clicked in search input
filter.addEventListener('keyup', filterItems);


//------FUNCTIONS------//
//Callback for form submit
function addItem(e) {
    //Dont submit to page
    e.preventDefault();
    //Get Input value
    var newItem = document.getElementById('item').value;
    //Call func that returns new item element
    var li = createItem(newItem);
    //call func that returns delete button for item
    var deleteBtn = createDeleteButton();
    //add delete button inside li
    li.appendChild(deleteBtn);
    //Add the new list item to the dom
    itemList.appendChild(li);
}


//create the li for the new item
function createItem(newItem) {
    //create new list item <li> from input
    var li = document.createElement('li');
    //add class to list item
    li.className = 'list-group-item';
    //Add input value to li
    li.appendChild(document.createTextNode(newItem));
    return li;
}

//Create the delete button
function createDeleteButton() {
    //Create delete button for new item
    var delBtn = document.createElement('button');
    //add styles to the button
    delBtn.className = "btn btn-danger btn-sm float-right delete";
    delBtn.appendChild(document.createTextNode('X'))
    return delBtn;
}

//function to delete item when X is clicked
function removeItem(e){
    if(e.target.classList.contains('delete')){
        if(confirm('Are You Sure?')){
            var li = e.target.parentElement;
            itemList.removeChild(li);
        }
    }
}

//function to filter items
function filterItems(e){
    //convert input value to lowercase
    var text = e.target.value.toLowerCase();
    //get all LI's within itemlist
    var items = itemList.getElementsByTagName('li');
    Array.from(items).forEach((item) => {
        var itemName = item.firstChild.textContent.toLowerCase();
        if(itemName.indexOf(text) != -1){
            item.style.display = 'block';
        }else{
            item.style.display = 'none';
        }
    })
}




